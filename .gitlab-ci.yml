image: registry.gitlab.com/gitlab-org/gitlab-build-images:gitlab-operator-build-base

variables:
  # Configuration of K8s
  NAMESPACE: "n${CI_COMMIT_SHORT_SHA}"
  TAG: "${CI_COMMIT_SHORT_SHA}"
  HOSTSUFFIX: "n${CI_COMMIT_SHORT_SHA}"
  TLSSECRETNAME: "gitlab-ci-tls"
  # docker configuration
  DOCKER_DRIVER: overlay2
  DOCKER_HOST: tcp://docker:2375

stages:
  - prepare
  - test
  - release
  - functional_test
  - qa
  - cleanup
  - certification
  - report

.if_push_to_redhat: &if_push_to_redhat
  if: '$PUSH_TO_REDHAT == "true" '

.if_release_tag: &if_release_tag
  if: '$CI_COMMIT_TAG =~ /^v[0-9]+\.[0-9]+\.[0-9]+(-(rc|RC|beta)\d*)?$/'

.skip_if_release_tag: &skip_if_release_tag
  <<: *if_release_tag
  when: never

.skip_always: &skip_always
  if: '$CI_COMMIT_TAG == $CI_COMMIT_TAG'
  when: never

include:
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml
  - local: .gitlab-ci-templates.yml

default:
  interruptible: true

pull_charts:
  stage: prepare
  script: scripts/retrieve_gitlab_charts.sh
  artifacts:
    paths:
      - charts/

lint_code:
  extends: .cache
  stage: test
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:golangci-lint-alpine
  script: golangci-lint run --out-format code-climate | tee gl-code-quality-report.json | jq -r '.[] | "\(.location.path):\(.location.lines.begin) \(.description)"'
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
    paths:
      - gl-code-quality-report.json
  allow_failure: true
  rules:
    - <<: *skip_if_release_tag
    - if: '$CI_COMMIT_BRANCH'

.test_job:
  extends: .cache
  stage: test
  variables:
    HELM_CHARTS: "${CI_PROJECT_DIR}/charts"
    GITLAB_OPERATOR_ASSETS: "${CI_PROJECT_DIR}/hack/assets"
  before_script:
    - mkdir coverage
    - export CHART_VERSION=$(sed -n ${VERSION_INDEX}p CHART_VERSIONS)
  rules:
    - <<: *skip_if_release_tag
    - if: '$CI_COMMIT_BRANCH'

unit_tests:
  extends: .test_job
  script: /go/bin/ginkgo -skip 'controller' -cover -outputdir=coverage ./...
  parallel:
    matrix:
      - VERSION_INDEX: ["1", "2", "3"]

slow_unit_tests:
  extends: .test_job
  script: /go/bin/ginkgo -focus 'controller' -cover -outputdir=coverage ./...
  variables:
    GITLAB_OPERATOR_SHARED_SECRETS_JOB_TIMEOUT: "600"
  parallel:
    matrix:
      - VERSION_INDEX: ["1", "2", "3"]

.docker_build_job:
  extends: .cache
  stage: release
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
    # Update module cache so it can be saved in CI cache (only the dependencies required to build)
    - docker run -v "${GOPATH}:/go" -v "${CI_PROJECT_DIR}:/code" -w /code golang:1.14 go list ./...
  interruptible: false

build_branch_image:
  extends: .docker_build_job
  script:
    - docker build -t "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}" -t "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}" .
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}"
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"
  except:
    refs:
      - master
      - tags

build_tag_image:
  extends: .docker_build_job
  script:
    - docker build -t "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}" -t "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}" .
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}"
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"
  rules:
    - <<: *if_release_tag

build_latest_image:
  extends: .docker_build_job
  script:
    - docker build -t "${CI_REGISTRY_IMAGE}:latest" -t "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}" .
    - docker push "${CI_REGISTRY_IMAGE}:latest"
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"
  only:
    refs:
      - master
      - tags

build_bundle_image:
  extends: .docker_build_job
  script:
    - docker build -t "${CI_REGISTRY_IMAGE}/bundle" -f Dockerfile.bundle .
    - docker push "${CI_REGISTRY_IMAGE}/bundle"
  only:
    refs:
      - master
    changes:
      - bundle/**/*

functional_tests_4_6:
  extends: .functional_tests
  stage: functional_test
  before_script:
  - export KUBECONFIG="$KUBECONFIG_OCP_4_6"
  variables:
    DOMAIN: apps.ocp-ci-4623.k8s-ft.win
  rules:
  - <<: *skip_if_release_tag
  - if: '$CI_COMMIT_BRANCH'

functional_tests_4_7:
  extends: .functional_tests
  stage: functional_test
  before_script:
  - export KUBECONFIG="$KUBECONFIG_OCP_4_7"
  variables:
    DOMAIN: apps.ocp-ci-4717.k8s-ft.win
  rules:
  - <<: *skip_if_release_tag
  - if: '$CI_COMMIT_BRANCH'

qa_4_6:
  extends: .qa
  stage: qa
  variables:
    DOMAIN: apps.ocp-ci-4623.k8s-ft.win
    QA_ENVIRONMENT_URL: "https://gitlab-${HOSTSUFFIX}.${DOMAIN}"
  dependencies:
    - functional_tests_4_6
  rules:
    - <<: *skip_if_release_tag
    - if: '$CI_COMMIT_BRANCH'

qa_4_7:
  extends: .qa
  stage: qa
  variables:
    DOMAIN: apps.ocp-ci-4717.k8s-ft.win
    QA_ENVIRONMENT_URL: "https://gitlab-${HOSTSUFFIX}.${DOMAIN}"
  dependencies:
    - functional_tests_4_7
  rules:
    - <<: *skip_if_release_tag
    - if: '$CI_COMMIT_BRANCH'

functional_tests_cleanup_4_6:
  extends: .functional_tests_cleanup
  stage: cleanup
  before_script:
  - export KUBECONFIG="$KUBECONFIG_OCP_4_6"

functional_tests_cleanup_4_7:
  extends: .functional_tests_cleanup
  stage: cleanup
  before_script:
  - export KUBECONFIG="$KUBECONFIG_OCP_4_7"

certification_upload:
  stage: certification
  image: "registry.gitlab.com/gitlab-org/gitlab-omnibus-builder/ruby_docker:1.0.1"
  services:
    - docker:dind
  rules:
    - <<: *if_release_tag
    - <<: *if_push_to_redhat
  retry: 1
  before_script:
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
  script:
    - ruby scripts/push_to_redhat.rb "${CI_COMMIT_REF_NAME}"

secret_detection:
  variables:
    SECRET_DETECTION_HISTORIC_SCAN: "true"

issue-bot:
  stage: report
  image: registry.gitlab.com/gitlab-org/distribution/issue-bot:latest
  script: /issue-bot
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      when: on_failure
